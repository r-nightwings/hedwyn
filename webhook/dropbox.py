import json
import os
import threading

import dropbox
import dropbox.files
import redis
from flask import request, Response

from webhook import app, config


@app.route('/webhook/dropbox', methods=['GET'])
def verification():
    resp = Response(request.args.get('challenge'))
    resp.headers['Content-Type'] = 'text/plain'
    resp.headers['X-Content-Type-Options'] = 'nosniff'

    return resp


@app.route('/webhook/dropbox', methods=['POST'])
def webhook():
    body = request.get_json()
    app.logger.debug("Received form %s", json.dumps(body))

    for account in body['list_folder']['accounts']:
        threading.Thread(target=process_user, args=(account,)).start()

    return ''


def process_user(account):
    app.logger.info("Processing account %s", account)
    access_token = config['dropbox']['access_token']
    dropbox_dir = config['dropbox']['dir']
    local_dir = config['nightwings']['dir']

    redis_client = redis.Redis()
    cursor = redis_client.hget('cursors', account)

    dbx = dropbox.Dropbox(access_token)

    while True:
        if cursor is None:
            result = dbx.files_list_folder(path=dropbox_dir)
        else:
            cursor = cursor.decode('utf-8')
            result = dbx.files_list_folder_continue(cursor)

        for entry in result.entries:
            if (isinstance(entry, dropbox.files.DeletedMetadata)
                    or isinstance(entry, dropbox.files.FolderMetadata)
                    or not entry.path_lower.endswith('.qif')):
                continue

            dropbox_file = entry.path_lower
            app.logger.info("Processing file %s", dropbox_file)
            local_file = os.path.join(local_dir, entry.name)

            with open(local_file, 'wb') as f:
                metadata, res = dbx.files_download(path=dropbox_file)
                f.write(res.content)

        cursor = result.cursor
        redis_client.hset('cursors', account, cursor)

        if not result.has_more:
            break

    return
