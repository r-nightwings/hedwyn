import logging
import yaml

from flask import Flask

app = Flask(__name__)
app.url_map.strict_slashes = False
app.logger.setLevel(logging.DEBUG)

with open('webhook/config.yml') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

import webhook.dropbox
